class Message < ActiveRecord::Base

  belongs_to :user, counter_cache: true
  belongs_to :chat
  has_and_belongs_to_many :recipients, class_name: 'User'

  default_scope {
    order(created_at: :desc)
  }

  after_create  do
    # Mark new message as unread for all chat's member except author
    this_message_recipients = self.chat.users.where.not(id: self.user_id)
    self.recipients << this_message_recipients
  end


end
