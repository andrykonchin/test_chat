class User < ActiveRecord::Base

  has_many :messages
  has_and_belongs_to_many :chats
  has_and_belongs_to_many :unread_messages, class_name: 'Message'

  validates :name, presence: true, uniqueness: true


  after_initialize do
    self.messages_count ||= 0
  end

  def unread_messages_for(chat)
    self.unread_messages.where(chat: chat)
  end


  def unread_messages_count_for(chat)
    unread_messages_for(chat).count
  end

end
