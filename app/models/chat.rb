class Chat < ActiveRecord::Base

  has_many :messages
  has_and_belongs_to_many :users, dependent: :destroy
  has_many :unread_messages, through: :users


  accepts_nested_attributes_for :users

  validate :validate_users_count

  default_scope {
    order(id: :desc)
  }

  # Минимальное количество участников
  MIN_USERS_COUNT = 2

  private

    def validate_users_count
      if self.users.size < MIN_USERS_COUNT
        self.errors[:user_ids] << "Need at least #{MIN_USERS_COUNT} users for chat"
      end
    end

end
