
class MessagesController < ApplicationController
  helper_method :chat

  def index
    @messages = chat.messages
  end


  def create
    @message = Chat::CreateMessage.call(chat, current_user, message_params)
    # respond_to do |format|
    #   if @message.save
    #     format.json { render json: @message, status: :created }
    #   else
    #     format.json { render json: @message.errors, status: :unprocessable_entity }
    #   end
    # end
    render :show
  end


  def unread
    @messages = current_user.unread_messages_for(chat)
    # respond_to do |format|
    #   format.json { render json: @messages, status: :created }
    # end
    render :index
  end


  private


    def chat
      @chat ||= current_user.chats.find(params[:chat_id])
    end


    def message_params
      params.require(:message).permit(
        :text
      )
    end

end
