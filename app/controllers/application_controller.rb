class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  before_filter :ensure_authenticated_user

  helper_method :current_user

  private


    rescue_from Exception do |e|
      respond_to do |format|
        format.json {
          status = case e
          when ActiveRecord::RecordNotFound   then  :not_found
          else                                      :internal_server_error
          end
          if Rails.env.production?
            render json: {}, status: status
          else
            render json: { exception: e.class.name, msg: e.message }, status: status
          end
        }
      end
    end


    def ensure_authenticated_user
      head :unauthorized unless current_user
    end

    def current_user
      # ? token
      # session[:user_id]
      @current_user ||= params[:user_id_token] && User.find(params[:user_id_token])
    end


    # active_model_serializers 0.9 and rails 4 bug fixer
    # Details:
    # https://github.com/rails-api/active_model_serializers/issues/641
    def _render_with_renderer_json(json, options)
      serializer = build_json_serializer(json, options)

      if serializer
        super(serializer, options)
      else
        super(json, options)
      end
    end

end
