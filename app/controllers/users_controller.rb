
class UsersController < ApplicationController

  skip_before_filter :ensure_authenticated_user


  def index
    @users = User.all
    # respond_to do |format|
    #   format.json { render json: @users, status: :ok }
    # end
  end


  def create
    @user = User.new user_params
    @user.save
    # respond_to do |format|
    #   if @user.save
    #     format.json { render json: @user, status: :created }
    #   else
    #     format.json { render json: @user.errors, status: :unprocessable_entity }
    #   end
    # end
    render :show
  end


  def show
    @user = User.find(params[:id])
    # respond_to do |format|
    #   format.json { render json: { user: @user }, status: :ok }
    # end
  end


  private


    def user_params
      params.require(:user).permit(
        :name,
        :password  # ? если для авторизации нужен пароль, значит и при создании надо запрашивать
      )
    end

end
