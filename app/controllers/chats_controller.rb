
class ChatsController < ApplicationController

  def index
    @chats = current_user.chats.includes(:users)
  end


  def create
    @chat = Chat::Create.call(chat_params, current_user)
    render :show
  end


  def update
    chat.update_attributes(chat_params)
    render :show
  end


  def mark_as_read
    Chat::MarkAsRead.call(chat, current_user)
    render :show
    # respond_to do |format|
    #   format.json { render json: {}, status: :ok }
    # end
  end


  private


    def chat
      @chat ||= current_user.chats.find(params[:id])
    end


    def chat_params
      params.require(:chat).permit(
        :name,
        user_ids:  []
      )
    end

end
