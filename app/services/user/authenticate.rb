class User::Authenticate
  def self.call(login, password)
    User.where(name: login, password: password).first
  end
end
