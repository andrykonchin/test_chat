class Chat::Create
  def self.call(params, creator)
    creator.chats.create(params).tap do |chat|
      chat.users << creator
    end
  end
end
