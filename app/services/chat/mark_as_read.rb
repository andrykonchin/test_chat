class Chat::MarkAsRead
  def self.call(chat, user)
    user.unread_messages.where(chat_id: chat.id).delete_all
  end
end
