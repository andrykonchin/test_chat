class Chat::CreateMessage
  def self.call(chat, creator, params)
    chat.messages.build(params). tap do |message|
      message.chat_id = chat.id
      message.save
    end
  end
end
