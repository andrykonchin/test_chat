
json.user {
  json.id user.id
  json.name user.name
  json.messages_count user.unread_messages.count
  json.chats user.chat_ids
}
