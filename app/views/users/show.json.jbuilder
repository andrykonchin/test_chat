
if @user.errors.empty?
  json.partial! 'user', user: @user
else
  json.errors do
    json.array! @user.errors
  end
end
