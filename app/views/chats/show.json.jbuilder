
if chat.errors.empty?
  json.partial! 'chat', chat: chat
else
  json.errors do
    json.array! chat.errors
  end
end
