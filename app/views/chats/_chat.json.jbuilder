
json.chat {
  json.id chat.id
  json.name chat.name
  json.users_ids chat.user_ids
  json.unread_messages_count current_user.unread_messages_count_for(chat)
}
