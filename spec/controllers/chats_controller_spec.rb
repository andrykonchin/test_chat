#encoding: utf-8

require 'rails_helper'

describe ChatsController do
  let(:ivan) { create(:user) }
  let(:petr) { create(:user) }
  let(:olga) { create(:user) }

  describe 'GET #index' do
    let(:ivan_and_olga_chat) { create(:chat, users: [ivan, olga]) }
    let(:petr_and_olga_chat) { create(:chat, users: [petr, olga]) }
    let(:common_chat)        { create(:chat, users: [petr, olga, ivan]) }

    it 'return all chats list' do
      get :index, user_id_token: olga.id, format: :json
      expect(assigns(:chats)).to match_array [ivan_and_olga_chat, petr_and_olga_chat, common_chat]
    end

    it 'renders the :index template' do
      get :index, user_id_token: olga.id, format: :json
      expect(response).to render_template :index
    end
  end


  describe 'POST #create' do
    context 'with valid attributes' do
      it 'saves the chat in the database' do
        expect{
          post :create, user_id_token: ivan.id, chat: { name: 'New chat', user_ids: [olga.id, petr.id] }, format: :json
        }.to change(Chat, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      let!(:ivan) { create(:user) }
      it 'don''t saves the chat with too few users count' do
        expect{
          post :create, user_id_token: ivan.id, chat: { name: 'Chat without other users', user_ids: [] }, format: :json
        }.to_not change(User, :count)
      end
    end
  end


  context 'change existing chat' do
    let(:ivan_and_petr_chat) { create(:chat, users: [ivan, petr]) }

    describe 'PATCH #update' do
      context 'valid attributes' do
        it 'changes chat name' do
          patch :update, id: ivan_and_petr_chat, user_id_token: ivan.id, chat: { name: 'Football' }, format: :json
          ivan_and_petr_chat.reload
          expect(ivan_and_petr_chat.name).to eq('Football')
        end

        it 'changes chat users' do
          patch :update, id: ivan_and_petr_chat, user_id_token: ivan.id, chat: { user_ids: [ivan.id, petr.id, olga.id] }, format: :json
          ivan_and_petr_chat.reload
          expect(ivan_and_petr_chat.users).to match_array [ivan, petr, olga]
        end
      end

      context 'invalid attributes' do
        it 'don''t changes chat name of users list if user not in chat' do
          patch :update, id: ivan_and_petr_chat, user_id_token: olga.id, chat: { name: 'Cats and flowers', user_ids: [ivan.id, petr.id, olga.id] }, format: :json
          ivan_and_petr_chat.reload
          expect(ivan_and_petr_chat.name).to_not eq('Cats and flowers')
          expect(ivan_and_petr_chat.name).to_not match_array [ivan, petr, olga]
        end
      end
    end


    describe 'PATCH #mark_as_read' do
      it 'mark chat (users messages in chat) as read only for this user' do
        create(:message, chat: ivan_and_petr_chat, user: ivan)
        create(:message, chat: ivan_and_petr_chat, user: petr)

        patch :mark_as_read, id: ivan_and_petr_chat, user_id_token: petr.id, format: :json
        expect(petr.unread_messages_count_for(ivan_and_petr_chat)).to be_zero
        expect(ivan.unread_messages_count_for(ivan_and_petr_chat)).to eq 1
      end
    end
  end

end
