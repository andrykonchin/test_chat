#encoding: utf-8

require 'rails_helper'

describe UsersController do

  describe 'GET #index' do
    it 'return all users list' do
      ivan = create(:user)
      petr = create(:user)
      get :index, format: :json
      expect(assigns(:users)).to match_array [ivan, petr]
    end

    it 'renders the :index template' do
      get :index, format: :json
      expect(response).to render_template :index
    end
  end


  describe 'POST #create' do
    context 'with valid attributes' do
      it 'saves the new user in the database' do
        expect{
          post :create, user: attributes_for(:user), format: :json
        }.to change(User, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'don''t saves the user with existing name in the database' do
        post :create, user: attributes_for(:user, name: 'Same Name'), format: :json
        expect{
          post :create, user: attributes_for(:user, name: 'Same Name'), format: :json
        }.to_not change(User, :count)
      end

      it 'don''t saves the user without name in the database' do
        expect{
          post :create, user: attributes_for(:user, name: nil), format: :json
        }.to_not change(User, :count)
      end
    end
  end


  describe 'GET #show' do
    it 'returns requested @user' do
      user = create(:user)
      get :show, id: user, format: :json
      expect(assigns(:user)).to eq user
    end

    it 'renders the :show template' do
      user = create(:user)
      get :show, id: user, format: :json
      expect(response).to render_template :show
    end
  end

end
