#encoding: utf-8

require 'rails_helper'

describe MessagesController do
  let(:ivan) { create(:user) }
  let(:petr) { create(:user) }
  let(:olga) { create(:user) }

  let(:ivan_and_olga_chat) { create(:chat, users: [ivan, olga]) }
  let(:petr_and_olga_chat) { create(:chat, users: [petr, olga]) }
  let(:common_chat)        { create(:chat, users: [petr, olga, ivan]) }

  context 'receive message' do

    describe 'GET #index' do
      it 'return all messages in chat for user' do
        msg_from_ivan = create(:message, user: ivan, chat: common_chat)
        msg_from_petr = create(:message, user: petr, chat: common_chat)
        get :index, chat_id: common_chat, user_id_token: olga.id, format: :json
        expect(assigns(:messages)).to match_array [msg_from_ivan, msg_from_petr]
      end

      it 'renders the :index template' do
        get :index, chat_id: common_chat, user_id_token: olga.id, format: :json
        expect(response).to render_template :index
      end
    end

    describe 'GET #unread' do
      it 'returns all unread messages for user in chat' do
        Chat::MarkAsRead.call(common_chat, olga)
        new_msg_from_petr = create(:message, user: petr, chat: common_chat)
        get :unread, chat_id: common_chat, user_id_token: olga.id, format: :json
        expect(assigns(:messages)).to match_array [new_msg_from_petr]
      end
    end
  end


  describe 'POST #create' do
    it 'saves the new message for chat in the database' do
      expect{
        post :create, chat_id: common_chat, user_id_token: olga.id, message: { text: 'Hi, guys!' }, format: :json
      }.to change(Message, :count).by(1)
    end
  end

end
