#encoding: utf-8

require 'rails_helper'

describe SessionsController do

  describe 'POST #create' do
    context 'valid attributes' do
      it 'create new session or token (201 code)' do
        user = create(:user)
        post :create, user: { name: user.name, password: user.password }, format: :json
        expect(response.code).to eq "201"
      end
    end

    context 'invalid attributes' do
      it 'fails to create session (401 code)' do
        user = create(:user)
        post :create, user: { name: user.name, password: 'wrong password' }, format: :json
        expect(response.code).to eq "401"
      end
    end
  end

end
