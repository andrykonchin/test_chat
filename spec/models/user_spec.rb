#encoding: utf-8

require 'rails_helper'

describe User do

  it 'может зарегистрироваться введя имя, если имя не уникальное то пользователю получает уведомление об этом' do
    create(:user, name: 'Same Name')
    user = build(:user, name: 'Same Name')
    user.valid?
    expect(user.errors[:name]).to include('has already been taken')
  end


  it 'может авторизоваться введя свое имя и пароль' do
    user_attributes = attributes_for(:user)
    user = User.create(user_attributes)
    authorized_user = User::Authenticate.call(user_attributes[:name], user_attributes[:password])
    expect(authorized_user).to eq user
  end

  context 'редактирование чата' do

    before :each do
      @chat_creator = create(:user)
      @chat_guest1 = create(:user)
      @chat = create(:chat, user_ids: [@chat_creator.id, @chat_guest1.id])
    end

    it 'может получить список чатов в которых он принимает участие' do
      expect(@chat_guest1.chats).to eq [@chat]
    end


    it 'может написать сообщение в чат, в котором он участвует' do
      msg = build(:message, user: @chat_creator, chat: @chat)
      expect(msg).to be_valid
    end


    it 'может получить список непрочитанных сообщений' do
      msg = create(:message, user: @chat_creator, chat: @chat)
      expect(@chat_guest1.unread_messages_for(@chat)).to include(msg)
    end


    it 'может отметить чат как прочитанный**' do
      msg = create(:message, user: @chat_creator, chat: @chat)
      unread_messages_count_before = @chat_guest1.unread_messages_count_for(@chat)
      Chat::MarkAsRead.call(@chat, @chat_guest1)
      unread_messages_count_after = @chat_guest1.unread_messages_count_for(@chat)
      expect(unread_messages_count_after).to be_zero
    end

  end


end
