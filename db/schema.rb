# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160710194921) do

  create_table "chats", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "chats_users", id: false, force: :cascade do |t|
    t.integer "chat_id"
    t.integer "user_id"
  end

  add_index "chats_users", ["chat_id"], name: "index_chats_users_on_chat_id"
  add_index "chats_users", ["user_id"], name: "index_chats_users_on_user_id"

  create_table "messages", force: :cascade do |t|
    t.string   "text",       null: false
    t.integer  "user_id"
    t.integer  "chat_id"
    t.datetime "created_at", null: false
  end

  add_index "messages", ["chat_id"], name: "index_messages_on_chat_id"
  add_index "messages", ["user_id"], name: "index_messages_on_user_id"

  create_table "messages_users", id: false, force: :cascade do |t|
    t.integer "message_id"
    t.integer "user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string  "name",           null: false
    t.integer "messages_count"
    t.string  "password"
  end

end
